import json
import urllib.request
import hashlib

import rdflib
from rdflib import Literal, URIRef, BNode
from rdflib.namespace import FOAF, RDF, DC, DOAP

from . import secrets

META = rdflib.Namespace('http://pafcu.fi/ns/meta/')
def triple_uri(triple):
	return URIRef('triple:sha1:%s'%hashlib.sha1(bytes(str(triple),'utf-8')).hexdigest())

def get_profile(gplus_id):
	g = rdflib.Graph()

	base_url = 'https://www.googleapis.com/plus/v1'
	user_url = '%s/people/%s?key=%s'%(base_url, gplus_id, secrets.api_key)
	with urllib.request.urlopen(user_url) as f:
		profile = json.loads(f.read().decode('utf-8'))

	user_uri = URIRef(profile['url'])
	
	g.add((user_uri, FOAF.name, Literal(profile['displayName'])))
	g.add((user_uri, FOAF.givenName, Literal(profile['name']['givenName'])))
	g.add((user_uri, FOAF.familyName, Literal(profile['name']['familyName'])))
	g.add((user_uri, FOAF.gender, Literal(profile['gender'])))

	for triple in g:
		g.add((triple_uri(triple), META.sourceService, URIRef('https://plus.google.com/')))

	return g

